<?php 
    include '../php/templates/_headerPartial.php';
    
    // get categories
//    $sql = "SELECT * FROM Food";
//    $food = $conn->query($sql);
//
//    $conn->close();
    
    // compiled lang: java, c#
    // script lang: javascript, php
    
    //print_r($conn);
?>

<?php
    $fileName = 'foodImage';
    $uploadPath = "/restaurantmanager/img/";
    $message = null;
    function moveUploadedFile($fileName, $uploadPath, $foodId) {
        $conn = null;
        if($conn == null) {
            $conn = getConnection();
        }  
        
        // upload image
        $target_dir = $_SERVER['DOCUMENT_ROOT'] . $uploadPath;
        $target_name = $_FILES[$fileName]["name"];
        $target_file = $target_dir . $target_name;

        if (move_uploaded_file($_FILES[$fileName]["tmp_name"], $target_file)) {
            
            echo "The file ". basename( $_FILES[$fileName]["name"]). " has been uploaded.";
            
            // save into db
            $sql = "INSERT into Image(Url, FoodId) values ('/img/$target_name', $foodId)";
            echo $sql;
            if ( $conn->query($sql) ) {
                $message = "Saved into db";
                
            } else {
                echo 'error when saving into db';
            }
            
        } else {
            echo "Sorry, there was an error uploading your file.";
            $message = "error when saving into db";
        }
    }
   
    
    if($_SERVER['REQUEST_METHOD'] == "POST") {
        
        $foodId = $_POST['id'];
        
        moveUploadedFile($fileName, $uploadPath, $foodId);
        
        header("Location: " . $prefix . "/php/food.php?message=$message");
    }
?>


<form action="food_add_image.php" method="post" enctype="multipart/form-data">
    <input type="hidden" name="id" value="<?php echo $_GET['id']; ?>" />
    <div class="form-group">
      <label for="email">Select image to upload:</label>
      <input type="file" name="foodImage" class="form-control" id="fileToUpload">
    </div>

    <input type="submit" value="Upload Image" name="submit" class="btn btn-default">
</form>


<?php 
    include '../php/templates/_footerPartial.php';
?>