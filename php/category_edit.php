<?php 
    include '../php/templates/_headerPartial.php';
?>

<?php
    $category = null;

    if ($_SERVER['REQUEST_METHOD'] === 'GET') {
        
        if(isset($_GET["id"])) {
            $id = $_GET["id"];

            $sql = "select * from Category where CategoryId = $id";

            $category = $conn->query($sql)->fetch_array(MYSQLI_ASSOC);

            print_r($category);
        }
    }
    
   
    
        if(isset($_POST["categoryId"])) {

            $id = $_POST["categoryId"];
            $newName = $_POST["category"];

            $sql = "update Category set Name = '" . $newName . "' where CategoryId = " . $id;

             if($conn->query($sql) == true) {
                echo 'success updating category';

            } else {
                echo 'failed updating new category';
                echo $conn->error;
            }
        }
    
?>

<form class="form-horizontal" action="category_edit.php" method="POST">
  <div class="form-group">
    <input type="hidden" name="categoryId" placeholder="" 
             value="<?PHP echo $category["CategoryId"] ?>">
    
    <label class="control-label col-sm-2" for="category">Category Name:</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" id="category" name="category" placeholder="" 
               value="<?PHP echo $category["Name"] ?>">
    </div>
  </div>
 
  <div class="form-group"> 
    <div class="col-sm-offset-2 col-sm-10">
      <button type="submit" class="btn btn-default">Submit</button>
    </div>
  </div>
</form>

<?php 
    include '../php/templates/_footerPartial.php';
?>
