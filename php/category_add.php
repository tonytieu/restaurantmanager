<?php 
    include '../php/templates/_headerPartial.php';
    
    if(isset($_POST["category"])) {
        
        // get data from form values
        $newCategory = $_POST["category"];
        
        // insert into db
        $sql = "insert into Category(name) values('$newCategory')";
        
        if($conn->query($sql) == true) {
            echo 'success inserting new category';
            
        } else {
            echo 'failed inserting new category';
            echo $conn->error;
        }
    }
    
?>

<h1>Add New Food</h1>

<form class="form-horizontal" action="category_add.php" method="POST">
  <div class="form-group">
    <label class="control-label col-sm-2" for="category">Category Name:</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" id="category" name="category" placeholder="">
    </div>
  </div>
 
  <div class="form-group"> 
    <div class="col-sm-offset-2 col-sm-10">
      <button type="submit" class="btn btn-default">Submit</button>
    </div>
  </div>
</form>

<?php 
    include '../php/templates/_footerPartial.php';
?>