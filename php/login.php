
<?php 
    include '../php/templates/_headerPartial.php';
?>

<?php
    $error = "";
    $email = null;
    $pwd = null;
    $rememberMe = null;

    if($_SERVER["REQUEST_METHOD"] == "POST") {
        $email = $_POST["email"];
        $pwd = $_POST["pwd"];
        if(isset($_POST["rememberMe"])) {
            $rememberMe = $_POST["rememberMe"];
        }

        
        // check in db
        $sql = "select Email, Role.Name 'RoleName' from Account 
                    join Role
                    on Account.RoleId = Role.RoleId 
                where Email like '$email' and Password like '$pwd'";
        
        
        $result = $conn->query($sql);
        $arr = $result->fetch_assoc();
        
        if(count($arr) == 0) {
            $error = "Either email or password is wrong";
        } else {
            $roleName = $arr["RoleName"];
            //echo $roleName;
            
            if($rememberMe) {
                setcookie("loggedinemail", $email, time() + (86400 * 30));
                
                if($roleName == "Admin") {
                    setcookie("logginedrolename", $roleName, time() + (86400 * 30));
                    
                }
            } else {
                $_SESSION["loggedinemail"] = $email;
                if($roleName == "Admin") {
                    $_SESSION["logginedrolename"] = $roleName;
                }
            }
            
            header("Location: " . $prefix);
        }
    }
    
//    setcookie("cookiekey", "cookievalue", time() + (86400 * 30));
//    $_SESSION["sessionkey"] = "sessionvalue";
    
    //echo $_SESSION["sessionkey"];
    
?>

<h1>Login</h1>

<p>
    <?php 
        echo $error; 
    ?>
</p>

<form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
  <div class="form-group">
    <label for="email">Email address:</label>
    <input type="email" class="form-control" id="email" name="email" value="<?php echo $email; ?>">
  </div>
  <div class="form-group">
    <label for="pwd">Password:</label>
    <input type="password" class="form-control" id="pwd" name="pwd" value="<?PHP echo $pwd ?>">
  </div>
  <div class="checkbox">
      <label><input type="checkbox" name="rememberMe"> Remember me</label>
  </div>
  <button type="submit" class="btn btn-default">Submit</button>
</form>

<?php 
    include '../php/templates/_footerPartial.php';
?>