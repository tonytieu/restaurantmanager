<?php 
    include '../php/templates/_headerPartial.php';
    
    // get categories
    $sql = "SELECT * FROM Category";
    $categories = $conn->query($sql);
    
    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        $Name = "Default name";
        $Price = null;
        $Description = null;
        $CategoryId = null;
        
        if(isset($_POST["Name"])) {
            // get data from form values
            $Name = $_POST["Name"];
        }
        
        if(isset($_POST["Price"])) {
            // get data from form values
            $Price = $_POST["Price"];
        }
        
        if(isset($_POST["Description"])) {
            // get data from form values
            $Description = $_POST["Description"];
        }
        
        if(isset($_POST["CategoryId"])) {
            // get data from form values
            $CategoryId = $_POST["CategoryId"];
        }
        
        $sql = null;
        
        if($CategoryId == null) {
            $sql = "insert into Food(Name, Price, Description) values('$Name', $Price, '$Description')";

        } else {
            $sql = "insert into Food(Name, Price, Description, CategoryId) values('$Name', $Price, '$Description', $CategoryId)";
        }
        // insert into db
        
        echo $sql;
        
        if($conn->query($sql) == true) {
            echo 'success inserting new food';

        } else {
            echo 'failed inserting new food';
            echo $conn->error;
        }
    }
    
    
    
?>

<h1>Add New Category</h1>

<form class="form-horizontal" action="food_add.php" method="POST">
  <div class="form-group">
    <label class="control-label col-sm-2" for="Name">Name:</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" id="Name" name="Name" placeholder="">
    </div>
  </div>
    
  <div class="form-group">
    <label class="control-label col-sm-2" for="Price">Price:</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" id="Price" name="Price" placeholder="">
    </div>
  </div>
    
  <div class="form-group">
    <label class="control-label col-sm-2" for="Description">Description:</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" id="Description" name="Description" placeholder="">
    </div>
  </div>
    
    <div class="form-group">
    <label class="control-label col-sm-2" for="CategoryId">CategoryId:</label>
    <div class="col-sm-10">
        <select class="form-control" name="CategoryId">
            <option value=""></option>
            <?php 
                while($cat = $categories->fetch_assoc()) {
                    echo '<option value="' . $cat["CategoryId"].'">' . $cat["Name"] . '</option>';
                }
            ?>
            
        </select>
        <!--<input type="text" class="form-control" id="CategoryId" name="CategoryId" placeholder="">-->
    </div>
  </div>
 
  <div class="form-group"> 
    <div class="col-sm-offset-2 col-sm-10">
      <button type="submit" class="btn btn-default">Submit</button>
    </div>
  </div>
</form>

<?php 
    include '../php/templates/_footerPartial.php';
?>