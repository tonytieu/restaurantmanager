
<?php 
    include '../php/templates/_headerPartial.php';
    
    // get categories
    $sql = "SELECT * FROM Food";
    $food = $conn->query($sql);

    $conn->close();
    
    if(isset($_GET["message"])) {
        echo '<div class="alert alert-success">
                '.$_GET["message"].'
                </div>';
    }
    
?>


<script>
    setTimeout(function(){
        $(".alert").hide();
    }, 3000);
</script>

<h1>Food Management</h1>

<a href="<?php echo $prefix; ?>/php/food_add.php" class="btn btn-default">Add </a>

<table class="table table-bordered table-condensed table-hover table-responsive table-striped">
    <thead>
        <tr>
            <th>
                Id
            </th>
            <th>
                Name
            </th>
            <th>
                Price
            </th>
            <th>
                Description
            </th>
            <th>
                CategoryId
            </th>
            <th>
                Action
            </th>
        </tr>
    </thead>
    <tbody>
        <?php 
            while($row = $food->fetch_assoc()) {   
                echo '<tr>' .
                        "<td>$row[FoodId]</td>" .
                        "<td>$row[Name]</td>" .
                        "<td>$row[Price]</td>" .
                        "<td>$row[Description]</td>" .
                        "<td>$row[CategoryId]</td>" .
                        '<td><a href="/restaurantmanager/php/category_edit.php?id='.$row['FoodId'].'"  class="btn btn-default">Edit</a>'
                        . '<a href="/restaurantmanager/php/category_delete.php?id='. $row['FoodId'].'" class="btn btn-danger">Delete</a>'
                        . '<a href="/restaurantmanager/php/food_add_image.php?id='. $row['FoodId'].'" class="btn btn-info">Add image</a>'
                        . '<a href="/restaurantmanager/php/food_view_image.php?id='. $row['FoodId'].'" class="btn btn-info">View image</a>'
                        . '</td>' .
                    '</tr>';
            }
        ?>
        
    </tbody>
</table>



<?php 
    include '../php/templates/_footerPartial.php';
?>