
<?php 
    include '../php/templates/_headerPartial.php';
?>

<?php
    // get categories
    $sql = "SELECT * FROM Category";
    $categories = $conn->query($sql);

    $conn->close();

?>

<h1>Categories</h1>

<a href="/restaurantmanager/php/category_add.php" class="btn btn-default">Add </a>

<table class="table table-bordered table-condensed table-hover table-responsive table-striped">
    <thead>
        <tr>
            <th>
                Id
            </th>
            <th>
                Name
            </th>
            <th>
                Action
            </th>
        </tr>
    </thead>
    <tbody>
        <?php 
            while($row = $categories->fetch_assoc()) {   
                echo '<tr>' .
                        "<td>$row[CategoryId]</td>" .
                        "<td>$row[Name]</td>" .
                        '<td><a href="/restaurantmanager/php/category_edit.php?id='.$row['CategoryId'].'"  class="btn btn-default">Edit</a>'
                        . '<a href="/restaurantmanager/php/category_delete.php?id='. $row['CategoryId'].'" class="btn btn-danger">Delete</a></td>' .
                    '</tr>';
            }
        ?>
        
    </tbody>
</table>

<?php 
    include '../php/templates/_footerPartial.php';
?>