
<?php
    // future proof
    include  '../php/templates/_headerPartial.php';
?>

<?php
    $noOfItems = 4;
    $pageNo = 0;

    if(isset($_GET["pageNo"])) {
        $pageNo = $_GET["pageNo"];
    }

    // get categories
    $sql = "SELECT * FROM Category";
    $categories = $conn->query($sql);

    // multithreading
    //$conn->close();
    
    function printCategoryLevel($CategoryId) {
        $conn = getConnection();
        
        $subsql = "SELECT * FROM Category Where ParentId = " . $CategoryId;
                                               
        $subcategories = $conn->query($subsql);
        
        if ($subcategories->num_rows > 0) {
        
            echo '<ul>';
            while($subrow = $subcategories->fetch_assoc()) {
                echo '<li>';
                echo $subrow["Name"];

                    printCategoryLevel($subrow["CategoryId"]);
                
                echo '</li>';

            }
            echo '</ul>';
        }
    }
    
    $sql = "select * from food";
    $foods = $conn->query($sql);
    
    $totalItems = mysqli_num_rows($foods);
    
    $noOfPages = $totalItems / $noOfItems;
    
    $foodARray = $foods->fetch_all(MYSQLI_ASSOC);
    
    $foodARray = array_slice($foodARray, $noOfItems * $pageNo, $noOfItems);
    
    //print_r($foodARray);
    
    $sql = 'SELECT f.CategoryId, c.Name, count(*) as NoOfProducts 
            FROM Food f
            join category c
                    on f.CategoryId = c.CategoryId
            group by f.CategoryId, c.Name
            ';
    $graphData = $conn->query($sql);
    
    $dictionary = array();
    while($row = $graphData->fetch_assoc()) {
        array_push($dictionary, array( "Name" => $row["Name"], "y" => $row["NoOfProducts"]));
        
    }
    
    print_r($dictionary);
    
    // 1: 0, 1, 2, 3
    // 2: 4, 5, 6, 7
    // 3: 8 9 10 11
    
    //echo $noOfPages;
    
?>

<style>
    @-webkit-keyframes load8 {
        0% {
          -webkit-transform: rotate(0deg);
          transform: rotate(0deg);
        }
        100% {
          -webkit-transform: rotate(360deg);
          transform: rotate(360deg);
        }
      }
      @keyframes load8 {
        0% {
          -webkit-transform: rotate(0deg);
          transform: rotate(0deg);
        }
        100% {
          -webkit-transform: rotate(360deg);
          transform: rotate(360deg);
        }
      }

    .spinner {
        -webkit-transform: translateZ(0);
        -ms-transform: translateZ(0);
        transform: translateZ(0);
        -webkit-animation: load8 1.1s infinite linear;
        animation: load8 1.1s infinite linear;
    }
</style>

<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/data.js"></script>
<script src="https://code.highcharts.com/modules/drilldown.js"></script>


  
<div class="container-fluid text-center">
  <div class="row content">
    <div class="col-sm-2 sidenav">
        <h3>Categories</h3>
        <ul>
            <?php 
                $sql = "SELECT * FROM Category Where ParentId is null";
                $parentcategories = $conn->query($sql);
                // recursive algorithm: On^n Ologn. O2^n.On^2. Input: n -> number of commands: n^2. 10 -> 100, 100 -> 10000 (notation)
                // business case: password finding: brute force:
                // 0123456789 A&
                // 2 chars: 0 - 1, 0- 2, 0 -3 ,- 4
                // 1 - 1, 1 - 2, 1- 3
                // // On. n = 10 -> 10, 20 -> 20
                // 0logn
                // // pros: de viet
                // cons: peformance
                while($row = $parentcategories->fetch_assoc()) {   
                    echo "<li>";
                    
                    echo $row["Name"];
                    printCategoryLevel($row["CategoryId"]);
//                        $subsql = "SELECT * FROM Category Where ParentId = " . $row["CategoryId"];
//                        
//                        
//                        
//                        $subcategories = $conn->query($subsql);
//
//                        echo '<ul>';
//                        while($subrow = $subcategories->fetch_assoc()) {
//                            echo '<li>';
//                            echo $subrow["Name"];
//                            
//                                $subsql = "SELECT * FROM Category Where ParentId = " . $subrow["CategoryId"];
//                                $subcategories1 = $conn->query($subsql);
//                                
//                                echo '<ul>';
//                                while($subrow1 = $subcategories1->fetch_assoc()) {
//                                    echo '<li>';
//                                    echo $subrow1["Name"];
//                                    echo '</li>';
//                                }
//                                echo '</ul>';
//                                
//                            echo '</li>';
//
//                        }
//                        echo '</ul>';
                    
                    echo '</li>';
                }
                
            ?>
        </ul>
        
        <div class="fb-share-button" data-href="https://developers.facebook.com/docs/plugins/" data-layout="button_count" data-size="small" data-mobile-iframe="true"><a class="fb-xfbml-parse-ignore" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fdevelopers.facebook.com%2Fdocs%2Fplugins%2F&amp;src=sdkpreparse">Share</a></div>
        
        <?php 
//            while($row = $categories->fetch_assoc()) {   
//                echo "<p><a href='#'>".$row["Name"]."</a></p>";
//            }
        ?>

    </div>
    <div class="col-sm-10 text-left">
        <h3>Product List</h3>
        <div class="row" id="productListDiv">
            <span class="glyphicon glyphicon-refresh spinner"></span>
            <?PHP
//            foreach($foodARray as $row) {
//                echo '<div class="col-md-3">';
//                  echo '<div class="panel panel-default">
//                            <div class="panel-body">'.$row["Name"].'</div>
//                          </div>';
//                  echo '</div>';
//            }
            ?>
        </div>
        <ul class="pagination">
            <?PHP 
                for($i = 0; $i < $noOfPages; $i++) {
                    if($i == $pageNo) {
                        echo '<li class="active"><a href"/restaurantmanager/php/home.php?pageNo='.$i.'" >'.($i + 1).'</a></li>';
                    } else {
                        echo '<li><a href="/restaurantmanager/php/home.php?pageNo='.$i.'">'.($i + 1).'</a></li>';
                    }
                }
            ?>
            
<!--            <li><a href="#">2</a></li>
            <li><a href="#">3</a></li>
            <li><a href="#">4</a></li>
            <li><a href="#">5</a></li>-->
          </ul>
    </div>

  </div>
</div>

<div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>

<script>
    $(document).ready(function(){
        //$(".pagination li").eq(0).addClass("active");
        
        /// use ajax  to get all products
        setTimeout(function(){
            $.get("/restaurantmanager/php/getAllProducts.php").done(function(d){
                console.log(d);
                $("#productListDiv").html(d);
            });
        }, 5000);
        
        
        var stageGraphData = <?php echo json_encode($dictionary); ?>;
        
        console.log(stageGraphData);
        
        var data = [];
        
        $.each(stageGraphData, function(index){
            console.log(stageGraphData[index]);
            data.push({
                name: stageGraphData[index].Name,
                y: parseInt(stageGraphData[index].y),
            });
        });
        
        console.log(data);
        
        Highcharts.chart('container', {
            chart: {
                type: 'column'
            },
            title: {
                text: 'categories with number of products'
            },

            xAxis: {
                type: 'category'
            },
            yAxis: {
                title: {
                    text: 'number of products'
                }

            },
            legend: {
                enabled: false
            },
            plotOptions: {
                series: {
                    borderWidth: 0,
                    dataLabels: {
                        enabled: true,
                        format: '{point.y:.1f}'
                    }
                }
            },

            tooltip: {
                headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}</b> of total<br/>'
            },

            series: [{
                name: 'NoOfProducts',
                colorByPoint: true,
                data: data
            }]
        });
    });
</script>


<?php 
    include '../php/templates/_footerPartial.php';
?>