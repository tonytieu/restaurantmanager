<?php

    function getConnection() {
        $servername = "localhost";
        $username = "root";
        $password = "";
        $dbname = "restaurantmanager";

        // Create connection
        $conn = new mysqli($servername, $username, $password, $dbname);

        // Check connection
        if ($conn->connect_error) {
            return null;
        }

        return $conn;
    }
    
    $prefix = "/restaurantmanager";
?>