<?php 
    include 'helper/utility.php';
    
    $conn = getConnection();
    
    if(!$conn) {
        die('not connected');
    }
    
    session_start();
    
?>

<!--<html>
    <head>
        <meta charset="UTF-8">
        <title>Home Page</title>
    </head>
    <body>
        
            <script>
                document.writeln("<h1>hompage js</h1>");
            </script>
            
            //<?php
//                // gloabl scope
//                $port = 33;
//                $port = "stasdfasd";
//                
//                function test($p) {
//                    return $p;
//                }
//                
//                echo '<br />';
//                
//                echo 'homepage php ' . test('9000000str');
//                
//                
//                echo '<br />';
//                
//                echo 'homepage php ' . $port;
//                
//                $colors = array("red", "green", "blue", "yellow"); 
//                
//                for($i = 0; $i < count($colors); $i++) {
//                    echo "$colors[$i] <br>";
//                    
//                }
//                
//                foreach ($colors as $value) {
//                    echo "$value <br>";
//                }
//                
//                // dictionary - associative array                
//                $form = array("email" => "asdfas@gmail.com", "age" => 25);
//                
//                if($form["username"]) {
//                    echo 'userName: ' . $form["username"];                   
//                } else {
//                    echo 'username cannot be empty';
//                }
//                
//            ?>

    </body>
</html>-->

<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  
  <link rel="stylesheet" href="<?php echo $prefix; ?>/plugins/bootstrap/css//bootstrap.min.css">
  <script src="<?php echo $prefix; ?>/plugins/jquery/jquery-3.1.1.min.js"></script>
  <script src="<?php echo $prefix; ?>/plugins/bootstrap/js/bootstrap.min.js"></script>
  
  <style>
    /* Remove the navbar's default margin-bottom and rounded borders */
    .navbar {
      margin-bottom: 0;
      border-radius: 0;
    }
    
    /* Set height of the grid so .sidenav can be 100% (adjust as needed) */
    .row.content {height: 450px}
    
    /* Set gray background color and 100% height */
    .sidenav {
      padding-top: 20px;
      background-color: #f1f1f1;
      height: 100%;
    }
    
    /* Set black background color, white text and some padding */
    footer {
      background-color: #555;
      color: white;
      padding: 15px;
    }
    
    /* On small screens, set height to 'auto' for sidenav and grid */
    @media screen and (max-width: 767px) {
      .sidenav {
        height: auto;
        padding: 15px;
      }
      .row.content {height:auto;}
    }
  </style>
</head>
<body>

<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">Logo</a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
          
        <li class="active"><a href="<?php echo $prefix; ?>/">Home</a></li>
        
        <?PHP if(isset($_SESSION["logginedrolename"]) || isset($_COOKIE["logginedrolename"])) { ?>
        <li><a href="<?php echo $prefix; ?>/php/categories.php">Categories</a></li>
        <li><a href="<?php echo $prefix; ?>/php/food.php">Food</a></li>
        <?PHP } ?>
        
        <li><a href="#">Contact</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
          <?PHP if(isset($_SESSION["loggedinemail"])) { ?>
            <li><a href="#">Hello <?PHP echo $_SESSION["loggedinemail"]; ?></a></li>
            <li><a href="<?PHP echo $prefix; ?>/php/logout.php">Logout</a></li>
          <?PHP } else if(isset($_COOKIE["loggedinemail"])) { ?>
            <li><a href="#">Hello <?PHP echo $_COOKIE["loggedinemail"]; ?></a></li>
            <li><a href="<?PHP echo $prefix; ?>/php/logout.php">Logout</a></li>
          <?PHP } else { ?>
            <li><a href="<?php echo $prefix; ?>/php/login.php"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
          <?PHP } ?>
      </ul>
    </div>
  </div>
</nav>