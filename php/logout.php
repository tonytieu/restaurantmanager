<?php 
    include '../php/templates/_headerPartial.php';
?>


<?php

   
    echo 'logout';
    
    if(isset($_SESSION["loggedinemail"])) {
        // remove all session variables
        session_unset(); 

        // destroy the session 
        session_destroy(); 
    } else if(isset($_COOKIE["loggedinemail"])) {
        setcookie("loggedinemail", "", time() - 3600);
    }
    
    header("Location: " . $prefix);
?>

